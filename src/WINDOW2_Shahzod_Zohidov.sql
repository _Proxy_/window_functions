-- Task:
--Identify the subcategories of products with consistently higher sales from 1998 to 2001 compared to the previous year. Follow the instructions below:
--Determine the sales for each subcategory from 1998 to 2001.
--Calculate the sales for the previous year for each subcategory.
--Identify subcategories where the sales from 1998 to 2001 are consistently higher than the previous year.
--Generate a dataset with a single column containing the identified prod_subcategory values.

 --Calculate total sales for each product subcategory and year
WITH SubcategorySales AS (
    SELECT
        p.prod_subcategory,
        t.calendar_year,
        SUM(s.amount_sold) AS total_sales
    FROM
        sh.sales s
            JOIN sh.products p ON s.prod_id = p.prod_id
            JOIN sh.times t ON s.time_id = t.time_id
    WHERE
        t.calendar_year BETWEEN 1998 AND 2001
    GROUP BY
        p.prod_subcategory, t.calendar_year
),--Calculate lagged (previous year) total sales for each product subcategory
     LaggedSubcategorySales AS (
         SELECT
             prod_subcategory,
             calendar_year,
             total_sales,
             LAG(total_sales, 1) OVER (PARTITION BY prod_subcategory ORDER BY calendar_year) AS prev_year_sales
         FROM
             SubcategorySales
     )
     -- Main query to select and display the results
SELECT
    prod_subcategory,
    calendar_year,
    total_sales,
    prev_year_sales
FROM
    LaggedSubcategorySales;
    
--SubcategorySales CTE:
--The purpose is to calculate the total sales for each product subcategory and year within the specified time range (1998-2001).
--The JOIN clause is used to connect the sales, products, and times tables based on their respective IDs.
--The WHERE clause filters the results to include only the specified calendar years.
--The GROUP BY clause groups the results by product subcategory and calendar year.
--The result is stored in the temporary table named SubcategorySales.


--LaggedSubcategorySales CTE:
--This CTE calculates the lagged (previous year) total sales for each product subcategory.
--The LAG function is used to retrieve the total sales from the previous year (LAG(total_sales, 1)).
--The PARTITION BY clause ensures that the lag is calculated separately for each product subcategory.
--The result is stored in the temporary table named LaggedSubcategorySales.

--Main Query:
--This query selects and displays the results from the LaggedSubcategorySales CTE.
--It includes columns for product subcategory, calendar year, total sales, and previous year's sales.
--The result is a list of product subcategories, their total sales for each year, and the corresponding sales from the previous year.
--This code essentially provides insights into the sales performance of product subcategories over the specified years, including a comparison with the previous year's sales.