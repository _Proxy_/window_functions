-- Task:
-- Retrieve customer information, total sales amount, and customer ranking within each channel for the specified time period.

-- SELECT Clause:
--      c.cust_id, c.cust_first_name, c.cust_last_name, ch.channel_id:
--          Selects customer information (ID, first name, last name) and the channel ID.
--      round(sum(s.amount_sold), 2) as total_amount:
--          Calculates the total sales amount for each customer, rounded to two decimal places.
--      rank() over (partition by ch.channel_id order by round(sum(s.amount_sold), 2) desc) as rnk:
--          Assigns a ranking to each customer within their respective channel based on the total sales amount.

-- FROM Clause:
--      Joins the customers, sales, and channels tables based on the customer ID and channel ID relationships.

-- WHERE Clause:
--      Filters the results to include only rows where the sales time falls within the specified years (1998, 1999, 2000, 2001).

-- GROUP BY Clause:
--      Groups the result set by customer ID and channel ID to calculate the total sales amount per customer within each channel.

-- ORDER BY Clause:
--      Orders the result set within each channel based on the total sales amount in descending order.

-- LIMIT Clause:
--      Limits the result set to include only the top 300 customers within each channel.

-- Final Result:
--      The final result includes customer information, total sales amount, and customer ranking within each channel for the specified time period.

select cust_id, cust_first_name||' '|| cust_last_name as customer_full_name, channel_id, total_amount,rnk as customer_rank
from (
    select 
        c.cust_id,
        c.cust_first_name,
        c.cust_last_name,
        ch.channel_id,
        round(sum(s.amount_sold), 2) as total_amount,
        rank() over (partition by ch.channel_id order by round(sum(s.amount_sold), 2) desc) as rnk
    from
        sh.customers c 
        join sh.sales s on c.cust_id = s.cust_id
        join sh.channels ch on s.channel_id = ch.channel_id  
    where extract(year from s.time_id) in (1998, 1999, 2000, 2001)
    group by
        c.cust_id,
        c.cust_first_name,
        c.cust_last_name,
        ch.channel_id)as ranked
	where rnk <= 300;
--This code retrieves customer information, total sales amount, and customer ranking within each channel for the specified time period (1998-2001), 
--with the results limited to the top 300 customers in each channel.